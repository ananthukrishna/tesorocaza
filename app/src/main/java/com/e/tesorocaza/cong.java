package com.e.tesorocaza;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;

import com.google.android.material.snackbar.Snackbar;

public class cong extends AppCompatActivity {
    private boolean doubleBackToExitPressedOnce = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cong);
    }
    public void onBackPressed()
    {
        if (doubleBackToExitPressedOnce)
        {
            finishAffinity();
            System.exit(0);
        }

        Snackbar snackbar  = Snackbar.make(findViewById(android.R.id.content), "Press back again to exit", Snackbar.LENGTH_LONG);
        snackbar.show();

        this.doubleBackToExitPressedOnce = true;

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 3000);
    }
}
