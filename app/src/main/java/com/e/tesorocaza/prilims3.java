package com.e.tesorocaza;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.Firebase;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class prilims3 extends AppCompatActivity {

    CountDownTimer countDownTimer;
    SharedPreferences sharedPreferences ;
    DatabaseReference reference,user;
    Firebase firebase;
    public static final String Firebase_Server_URL = "https://tesorocaza-fe7a5.firebaseio.com/";

    LinearLayout clue2,clue3,clue4,clue5;
    EditText answer;
    String Answer,savedid,savedname,time;
    Button submit;
    TextView timer;
    Integer Score;
    Integer clue;
    LinearLayout.LayoutParams layoutParams2;
    LinearLayout.LayoutParams layoutParams3;
    LinearLayout.LayoutParams layoutParams4;
    LinearLayout.LayoutParams layoutParams5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prilims3);

        sharedPreferences = getSharedPreferences("DATA", Context.MODE_PRIVATE);
        savedid = sharedPreferences.getString("id", null);
        savedname = sharedPreferences.getString("name", null);
        clue = sharedPreferences.getInt("clue3",1);

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("level",3);
        editor.apply();

        Toast.makeText(this, savedid, Toast.LENGTH_SHORT).show();

        Firebase.setAndroidContext(prilims3.this);
        firebase = new Firebase(Firebase_Server_URL);
        reference = FirebaseDatabase.getInstance().getReference();

        answer = findViewById(R.id.answer);
        submit = findViewById(R.id.submit);
        timer = findViewById(R.id.timer);
        setTitle("Prelims [Object - 3]");

        clue2 = findViewById(R.id.clue2);
        clue3 = findViewById(R.id.clue3);
        clue4 = findViewById(R.id.clue4);
        clue5 = findViewById(R.id.clue5);

        layoutParams2 = (LinearLayout.LayoutParams) clue2.getLayoutParams();
        layoutParams3 = (LinearLayout.LayoutParams) clue3.getLayoutParams();
        layoutParams4 = (LinearLayout.LayoutParams) clue4.getLayoutParams();
        layoutParams5 = (LinearLayout.LayoutParams) clue5.getLayoutParams();

        layoutParams2.height=0;
        layoutParams3.height=0;
        layoutParams4.height=0;
        layoutParams5.height=0;

        clue2.setLayoutParams(layoutParams2);
        clue3.setLayoutParams(layoutParams3);
        clue4.setLayoutParams(layoutParams4);
        clue5.setLayoutParams(layoutParams5);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (InternetConnection.checkConnection(prilims3.this))
                {

                    Answer = answer.getText().toString();
                    if (Answer.toUpperCase().equals(getResources().getString(R.string.prelimsanswer31)))
                    {
                        countDownTimer.cancel();

                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString("score3",String.valueOf(Score));
                        editor.putString("time3",time);
                        editor.apply();

                        UserData userData = new UserData();
                        userData.setId(savedid);
                        userData.setName(savedname);

                        userData.setScore1(sharedPreferences.getString("score1", null));
                        userData.setTime1(sharedPreferences.getString("time1", null));
                        userData.setScore2(sharedPreferences.getString("score2", null));
                        userData.setTime2(sharedPreferences.getString("time2", null));
                        userData.setScore3(sharedPreferences.getString("score3", null));
                        userData.setTime3(sharedPreferences.getString("time3", null));

                        user = reference.child("Users");
                        user.child(savedname+savedid).setValue(userData);

                        Toast.makeText(prilims3.this,"Correct answer", Toast.LENGTH_SHORT).show();

                        Intent intent = new Intent(prilims3.this,result.class);
                        startActivity(intent);

                    }
                    else
                    {
                        Toast.makeText(prilims3.this, "Wrong answer", Toast.LENGTH_SHORT).show();
                    }
                }
                else
                {
                    Toast.makeText(prilims3.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }
            }
        });

        if(clue == 1)
        {
            Score = 25;
            StartTimer();
        }
        else if(clue == 2)
        {
            Score = 20;
            setclue(clue);

        }
        else if(clue == 3)
        {
            Score = 15;
            setclue(clue);
        }
        else if(clue == 4)
        {
            Score = 10;
            setclue(clue);
        }
        else if(clue == 5)
        {
            Score = 5;
            setclue(clue);
        }
    }
    private void setclue(Integer clue)
    {
        if(clue == 2)
        {
            clue2.requestLayout();
            clue2.requestFocus(0);
            layoutParams2.height= ViewGroup.LayoutParams.WRAP_CONTENT;
            StartTimer();
        }
        else if(clue == 3)
        {
            clue2.requestLayout();
            clue2.requestFocus(0);
            layoutParams2.height= ViewGroup.LayoutParams.WRAP_CONTENT;
            clue3.requestLayout();
            clue3.requestFocus();
            layoutParams3.height= ViewGroup.LayoutParams.WRAP_CONTENT;
            StartTimer();
        }
        else if(clue == 4)
        {
            clue2.requestLayout();
            clue2.requestFocus(0);
            layoutParams2.height= ViewGroup.LayoutParams.WRAP_CONTENT;
            clue3.requestLayout();
            clue3.requestFocus();
            layoutParams3.height= ViewGroup.LayoutParams.WRAP_CONTENT;
            clue4.requestLayout();
            clue4.requestFocus();
            layoutParams4.height= ViewGroup.LayoutParams.WRAP_CONTENT;
            StartTimer();
        }
        else if(clue == 5)
        {
            clue2.requestLayout();
            clue2.requestFocus(0);
            layoutParams2.height= ViewGroup.LayoutParams.WRAP_CONTENT;
            clue3.requestLayout();
            clue3.requestFocus();
            layoutParams3.height= ViewGroup.LayoutParams.WRAP_CONTENT;
            clue4.requestLayout();
            clue4.requestFocus();
            layoutParams4.height= ViewGroup.LayoutParams.WRAP_CONTENT;
            clue5.requestLayout();
            clue5.requestFocus();
            layoutParams5.height= ViewGroup.LayoutParams.WRAP_CONTENT;
            StartTimer();
        }
    }
    public  void StartTimer()
    {
//
        countDownTimer = new CountDownTimer(30000,1000)
        {

            @Override
            public void onTick(long millisUntilFinished)
            {
                time = String.format(Locale.getDefault(),"%02d min : %02d sec",
                        TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished)%60,
                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished)%60);
                timer.setText(time);
            }

            @Override
            public void onFinish()
            {
                Score -= 5;
                if(Score == 20)
                {
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putInt("clue3",2);
                    editor.apply();
                    setclue(2);
                }
                else if(Score == 15)
                {
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putInt("clue3",3);
                    editor.apply();
                    setclue(3);
                }
                else if(Score == 10)
                {
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putInt("clue3",4);
                    editor.apply();
                    setclue(4);
                }
                else if(Score == 5)
                {
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putInt("clue3",5);
                    editor.apply();
                    setclue(5);
                }
                else if(Score == 0)
                {
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("score3","0");
                    editor.putString("time3","");
                    editor.apply();

                    UserData userData = new UserData();
                    userData.setId(savedid);
                    userData.setName(savedname);

                    userData.setScore1(sharedPreferences.getString("score1", null));
                    userData.setTime1(sharedPreferences.getString("time1", null));
                    userData.setScore2(sharedPreferences.getString("score2", null));
                    userData.setTime2(sharedPreferences.getString("time2", null));
                    userData.setScore3(sharedPreferences.getString("score3", null));
                    userData.setTime3(sharedPreferences.getString("time3", null));

                    user = reference.child("Users");
                    user.child(savedname+savedid).setValue(userData);

                    Intent intent = new Intent(prilims3.this,result.class);
                    startActivity(intent);
                }

            }
        }.start();
    }
    @Override
    public void onBackPressed()
    {
    }
}
