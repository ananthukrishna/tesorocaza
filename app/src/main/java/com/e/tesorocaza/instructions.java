package com.e.tesorocaza;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

public class instructions extends AppCompatActivity {

    String round;
    TextView rules;
    CheckBox checkBox;
    Button start;
    SharedPreferences sharedPreferences ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_instructions);
        rules = findViewById(R.id.rules);
        checkBox = findViewById(R.id.checkbox);
        start = findViewById(R.id.start);

        round = getIntent().getStringExtra("round");
        if(round.equals("Prilims"))
        {
            setTitle("Prelims Instructions");
            rules.setText(Html.fromHtml(getString(R.string.prilims)));
        }
        else if(round.equals("Finals"))
        {
            setTitle("Finals Instructions");
            rules.setText(Html.fromHtml(getString(R.string.finals)));
        }
        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if(checkBox.isChecked())
                {
                    if(round.equals("Prilims"))
                    {
                        sharedPreferences = getSharedPreferences("DATA", Context.MODE_PRIVATE);
                       int level  = sharedPreferences.getInt("level",1);
                        if(level == 1)
                        {
                            Intent intent = new Intent(instructions.this,prelims.class);
                            startActivity(intent);
                        }
                        else if(level == 2)
                        {
                            Intent intent2 = new Intent(instructions.this,prilims2.class);
                            startActivity(intent2);
                        }
                        else if(level == 3)
                        {
                            Intent intent3 = new Intent(instructions.this,prilims3.class);
                            startActivity(intent3);
                        }

                    }
                    else if(round.equals("Finals"))
                    {
                        Toast.makeText(instructions.this, "Scan the given QR code", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(instructions.this,finals.class);
                        startActivity(intent);
                    }
                }
                else
                {
                    Toast.makeText(instructions.this, "Check the box to continue", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
    @Override
    public void onBackPressed()
    {
    }
}
