package com.e.tesorocaza;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

public class finals extends AppCompatActivity {

    private static final int PERMISSION_REQUEST_CODE = 200;
    IntentIntegrator integrator;
    ImageButton scan;
    Button submit;
    EditText key;
    String z="0";

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_finals);
        setTitle("Finals");
        integrator = new IntentIntegrator(finals.this);
        scan = findViewById(R.id.scan);
        submit = findViewById(R.id.submit);
        key = findViewById(R.id.key);

        if (checkPermission())
        {

        } else {
            requestPermission();
        }

        scan.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE_TYPES);
                integrator.setResultDisplayDuration(0);//Text..
                integrator.setScanningRectangle(500, 500);
                integrator.setCameraId(0);
                integrator.initiateScan();
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                z = key.getText().toString();

                if(z.equals("")) {
                    Toast.makeText(finals.this, "Scan the QR code or Enter the key", Toast.LENGTH_SHORT).show();
                }
                else if(z.equals(getResources().getString( R.string.key1)))
                {
                    Intent intent = new Intent(finals.this,task1.class);
                    startActivity(intent);
                    finish();
                }
                else if(z.equals(getResources().getString(R.string.key2)))
                {

                    Intent intent = new Intent(finals.this,task2.class);
                    startActivity(intent);
                    finish();
                }
                else if(z.equals(getResources().getString(R.string.key3)))
                {

                    Intent intent = new Intent(finals.this,task3.class);
                    startActivity(intent);
                    finish();

                } else if(z.equals(getResources().getString(R.string.key4)))
                {

                    Intent intent = new Intent(finals.this,task4.class);
                    startActivity(intent);
                    finish();
                }
                else
                {
                    Toast.makeText(finals.this, "Wrong key", Toast.LENGTH_SHORT).show();
                    scan.setVisibility(View.VISIBLE);
                    key.setText("");
                }
            }
        });
    }

    private boolean checkPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            return false;
        }
        return true;
    }

    private void requestPermission() {

        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA},
                PERMISSION_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(getApplicationContext(), "Permission Granted", Toast.LENGTH_SHORT).show();

                    // main logic
                } else {
                    Toast.makeText(getApplicationContext(), "Permission Denied", Toast.LENGTH_SHORT).show();
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                                != PackageManager.PERMISSION_GRANTED) {
                            showMessageOKCancel("You need to allow access permissions",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                requestPermission();
                                            }
                                        }
                                    });
                        }
                    }
                }
                break;
        }
    }
    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(finals.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        IntentResult result = IntentIntegrator.parseActivityResult(requestCode,resultCode,data);
        if(result != null)
        {
            if(result.getContents()==null)
            {
                Toast.makeText(finals.this, "Cancelled", Toast.LENGTH_SHORT).show();
            }
            else
            {
                try
                {
                    key.setText(result.getContents());
                    z = key.getText().toString();
                    scan.setVisibility(View.INVISIBLE);
                }
                catch (Exception e)
                {

                }


            }
        }
        else
        {
            super.onActivityResult(requestCode, resultCode, data);
        }

    }
    @Override
    public void onBackPressed()
    {
    }
}
