package com.e.tesorocaza;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.widget.TextView;

import com.firebase.client.Firebase;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class result extends AppCompatActivity {
    TextView r;
    String savedid,savedname;
    SharedPreferences sharedPreferences ;
    Integer score1,score2,score3,total;
    DatabaseReference reference,user;
    Firebase firebase;
    public static final String Firebase_Server_URL = "https://tesorocaza-fe7a5.firebaseio.com/";
    private boolean doubleBackToExitPressedOnce = false;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        setTitle("Result");

        Firebase.setAndroidContext(result.this);
        firebase = new Firebase(Firebase_Server_URL);
        reference = FirebaseDatabase.getInstance().getReference();

        r = findViewById(R.id.r);
        sharedPreferences = getSharedPreferences("DATA", Context.MODE_PRIVATE);
        savedid = sharedPreferences.getString("id", null);
        savedname = sharedPreferences.getString("name", null);
        score1 = Integer.parseInt(sharedPreferences.getString("score1", null));
        score2 = Integer.parseInt(sharedPreferences.getString("score2", null));
        score3 = Integer.parseInt(sharedPreferences.getString("score3", null));
        total = score1 + score2 + score3;

        UserData userData = new UserData();
        userData.setId(savedid);
        userData.setName(savedname);

        userData.setScore1(sharedPreferences.getString("score1", null));
        userData.setTime1(sharedPreferences.getString("time1", null));
        userData.setScore2(sharedPreferences.getString("score2", null));
        userData.setTime2(sharedPreferences.getString("time2", null));
        userData.setScore3(sharedPreferences.getString("score3", null));
        userData.setTime3(sharedPreferences.getString("time3", null));
        userData.setTotal(String.valueOf(total));
        user = reference.child("Users");
        user.child(savedname+savedid).setValue(userData);

        r.setText("Your score is : "+String.valueOf(total));
    }
    @Override
    public void onBackPressed()
    {
        if (doubleBackToExitPressedOnce)
        {
            finishAffinity();
            System.exit(0);
        }

        Snackbar snackbar  = Snackbar.make(findViewById(android.R.id.content), "Press back again to exit", Snackbar.LENGTH_LONG);
        snackbar.show();

        this.doubleBackToExitPressedOnce = true;

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 3000);
    }
}
