package com.e.tesorocaza;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

public class task3 extends AppCompatActivity {
    LinearLayout clue1, clue2, clue3, clue4, clue5;
    EditText key;
    Button scan,submit;
    IntentIntegrator integrator;
    String z="0";
    int l=1;
    SharedPreferences sharedPreferences;
    LinearLayout.LayoutParams layoutParams1,layoutParams2,layoutParams3,layoutParams4,layoutParams5;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task3);

        sharedPreferences = getSharedPreferences("MyPref", MODE_PRIVATE);
        if (sharedPreferences.contains("level"))
        {
            l = sharedPreferences.getInt("level", 1);
        }
        integrator = new IntentIntegrator(task3.this);

        clue1 = findViewById(R.id.clue1);
        clue2 = findViewById(R.id.clue2);
        clue3 = findViewById(R.id.clue3);
        clue4 = findViewById(R.id.clue4);
        clue5 = findViewById(R.id.clue5);

        layoutParams1 = (LinearLayout.LayoutParams) clue1.getLayoutParams();
        layoutParams2 = (LinearLayout.LayoutParams) clue2.getLayoutParams();
        layoutParams3 = (LinearLayout.LayoutParams) clue3.getLayoutParams();
        layoutParams4 = (LinearLayout.LayoutParams) clue4.getLayoutParams();
        layoutParams5 = (LinearLayout.LayoutParams) clue5.getLayoutParams();

        setHeight();

        key = findViewById(R.id.key);
        scan = findViewById(R.id.scan);
        submit = findViewById(R.id.submit);

        scan.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE_TYPES);
                integrator.setResultDisplayDuration(0);//Text..
                integrator.setScanningRectangle(500, 500);
                integrator.setCameraId(0);
                integrator.initiateScan();
            }
        });

        submit.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                z = key.getText().toString();
                if (l == 1)
                {
                    if (z.equals(""))
                    {
                        Toast.makeText(task3.this, "Scan the QR code or Enter the key", Toast.LENGTH_SHORT).show();
                    }
                    else if (z.equals(getResources().getString(R.string.task3key1)))
                    {
                        l=2;

                        AlertDialog alertDialog = new AlertDialog.Builder(task3.this).create();
                        alertDialog.setTitle("Level 1 completed");
                        alertDialog.setMessage("Best Of Luck \uD83D\uDE0A\uD83D\uDE0A\uD83D\uDC4D\uD83D\uDC4D");
                        alertDialog.setCancelable(false);
                        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Level 2", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which)
                            {
                                setLevel(2);

                            }
                        });

                        alertDialog.show();
                    }
                    else
                    {
                        Toast.makeText(task3.this, "Wrong key", Toast.LENGTH_SHORT).show();
                        key.setText("");
                    }
                }
                else if(l==2)
                {
                    if (z.equals(""))
                    {
                        Toast.makeText(task3.this, "Scan the QR code or Enter the key", Toast.LENGTH_SHORT).show();
                    }
                    else if (z.equals(getResources().getString(R.string.task3key2)))
                    {
                        l=3;
                        AlertDialog alertDialog = new AlertDialog.Builder(task3.this).create();
                        alertDialog.setTitle("Level 2 completed");
                        alertDialog.setMessage("Best Of Luck \uD83D\uDE0A\uD83D\uDE0A\uD83D\uDC4D\uD83D\uDC4D");
                        alertDialog.setCancelable(false);
                        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Level 3", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which)
                            {
                                setLevel(3);
                            }
                        });

                        alertDialog.show();
                    }
                    else
                    {
                        Toast.makeText(task3.this, "Wrong key", Toast.LENGTH_SHORT).show();
                        key.setText("");
                    }
                }
                else  if(l==3)
                {
                    if (z.equals(""))
                    {
                        Toast.makeText(task3.this, "Scan the QR code or Enter the key", Toast.LENGTH_SHORT).show();
                    }
                    else if (z.equals(getResources().getString(R.string.task3key3)))
                    {
                        l=4;

                        AlertDialog alertDialog = new AlertDialog.Builder(task3.this).create();
                        alertDialog.setTitle("Level 3 completed");
                        alertDialog.setMessage("Best Of Luck \uD83D\uDE0A\uD83D\uDE0A\uD83D\uDC4D\uD83D\uDC4D");
                        alertDialog.setCancelable(false);
                        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Level 4", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which)
                            {
                                setLevel(4);

                            }
                        });

                        alertDialog.show();
                    }
                    else
                    {
                        Toast.makeText(task3.this, "Wrong key", Toast.LENGTH_SHORT).show();
                        key.setText("");
                    }
                }
                else if(l==4)
                {
                    if (z.equals(""))
                    {
                        Toast.makeText(task3.this, "Scan the QR code or Enter the key", Toast.LENGTH_SHORT).show();
                    }
                    else if (z.equals(getResources().getString(R.string.task3key4)))
                    {
                        l=5;
                        AlertDialog alertDialog = new AlertDialog.Builder(task3.this).create();
                        alertDialog.setTitle("Level 4 completed");
                        alertDialog.setMessage("Best Of Luck \uD83D\uDE0A\uD83D\uDE0A\uD83D\uDC4D\uD83D\uDC4D");
                        alertDialog.setCancelable(false);
                        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Level 5", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which)
                            {
                                setLevel(5);

                            }
                        });

                        alertDialog.show();

                    }
                    else
                    {
                        Toast.makeText(task3.this, "Wrong key", Toast.LENGTH_SHORT).show();
                        key.setText("");
                    }
                }

                else if(l==5)
                {
                    if (z.equals(""))
                    {
                        Toast.makeText(task3.this, "Scan the QR code or Enter the key", Toast.LENGTH_SHORT).show();
                    }
                    else if (z.equals(getResources().getString(R.string.keycommon)))
                    {
                        l=6;
                        setLevel(6);

                    }
                    else
                    {
                        Toast.makeText(task3.this, "Wrong key", Toast.LENGTH_SHORT).show();
                        key.setText("");
                    }
                }
            }
        });

        setLevel(l);

    }

    private void setHeight()
    {
        layoutParams1.height=0;
        layoutParams2.height=0;
        layoutParams3.height=0;
        layoutParams4.height=0;
        layoutParams5.height=0;

        clue1.setLayoutParams(layoutParams1);
        clue2.setLayoutParams(layoutParams2);
        clue3.setLayoutParams(layoutParams3);
        clue4.setLayoutParams(layoutParams4);
        clue5.setLayoutParams(layoutParams5);

    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        IntentResult result = IntentIntegrator.parseActivityResult(requestCode,resultCode,data);
        if(result != null) {
            if (result.getContents() == null)
            {
                Toast.makeText(this, "Cancelled", Toast.LENGTH_SHORT).show();
            }
            else {

                key.setText(result.getContents());
                z = key.getText().toString();
            }
        }
        else
        {
            super.onActivityResult(requestCode, resultCode, data);
        }

    }
    private void setLevel(int level) {
        if(level == 1)
        {
            setHeight();
            setTitle("Level 1");
            layoutParams1.height= ViewGroup.LayoutParams.WRAP_CONTENT;
            clue1.setLayoutParams(layoutParams1);
            clue1.requestLayout();
        }
        else if (level == 2){
            setHeight();
            key.setText("");
            setTitle("Level 2");
            layoutParams2.height= ViewGroup.LayoutParams.WRAP_CONTENT;
            clue2.setLayoutParams(layoutParams2);
            clue2.requestLayout();
        }
        else if (level == 3)
        {
            setHeight();
            key.setText("");
            setTitle("Level 3");

            layoutParams3.height= ViewGroup.LayoutParams.WRAP_CONTENT;
            clue3.setLayoutParams(layoutParams3);
            clue3.requestLayout();
        }
        else if (level == 4)
        {
            setHeight();
            key.setText("");
            setTitle("Level 4");

            layoutParams4.height= ViewGroup.LayoutParams.WRAP_CONTENT;
            clue4.setLayoutParams(layoutParams4);
            clue4.requestLayout();
        }
        else if (level == 5)
        {
            setHeight();
            key.setText("");
            setTitle("Level 5");

            layoutParams5.height= ViewGroup.LayoutParams.WRAP_CONTENT;
            clue5.setLayoutParams(layoutParams5);
            clue5.requestLayout();
        }
        else if (level == 6){
            Intent intent = new Intent(this,cong.class);
            startActivity(intent);
            finish();
        }
    }

    @Override
    public void onBackPressed()
    {
    }

    @Override
    protected void onPause()
    {
        super.onPause();
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("level", l);
        editor.apply();
    }
    @Override
    protected void onResume() {
        super.onResume();

    }
}
