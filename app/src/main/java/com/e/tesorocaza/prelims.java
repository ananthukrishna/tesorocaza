package com.e.tesorocaza;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.Firebase;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class prelims extends AppCompatActivity {

    CountDownTimer countDownTimer;
    SharedPreferences sharedPreferences ;
    DatabaseReference reference,user;
    Firebase firebase;
    Boolean timeout;

    public static final String Firebase_Server_URL = "https://tesorocaza-fe7a5.firebaseio.com/";

    LinearLayout clue2,clue3,clue4,clue5;
    EditText answer;
    String Answer,savedid,savedname,time;
    Button submit,next;
    TextView timer;
    Integer Score;
    Integer clue;
    LinearLayout.LayoutParams layoutParams2;
    LinearLayout.LayoutParams layoutParams3;
    LinearLayout.LayoutParams layoutParams4;
    LinearLayout.LayoutParams layoutParams5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prelims);
        timeout = true;

        sharedPreferences = getSharedPreferences("DATA", Context.MODE_PRIVATE);
        savedid = sharedPreferences.getString("id", null);
        savedname = sharedPreferences.getString("name", null);
        clue = sharedPreferences.getInt("clue1",1);



        Toast.makeText(this, savedid, Toast.LENGTH_SHORT).show();

        Firebase.setAndroidContext(prelims.this);
        firebase = new Firebase(Firebase_Server_URL);
        reference = FirebaseDatabase.getInstance().getReference();

        answer = findViewById(R.id.answer);
        submit = findViewById(R.id.submit);
        next = findViewById(R.id.next);
        timer = findViewById(R.id.timer);
        setTitle("Prelims [Object - 1]");

        clue2 = findViewById(R.id.clue2);
        clue3 = findViewById(R.id.clue3);
        clue4 = findViewById(R.id.clue4);
        clue5 = findViewById(R.id.clue5);

       layoutParams2 = (LinearLayout.LayoutParams) clue2.getLayoutParams();
       layoutParams3 = (LinearLayout.LayoutParams) clue3.getLayoutParams();
       layoutParams4 = (LinearLayout.LayoutParams) clue4.getLayoutParams();
       layoutParams5 = (LinearLayout.LayoutParams) clue5.getLayoutParams();

        layoutParams2.height=0;
        layoutParams3.height=0;
        layoutParams4.height=0;
        layoutParams5.height=0;

        clue2.setLayoutParams(layoutParams2);
        clue3.setLayoutParams(layoutParams3);
        clue4.setLayoutParams(layoutParams4);
        clue5.setLayoutParams(layoutParams5);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (InternetConnection.checkConnection(prelims.this)) {

                    Answer = answer.getText().toString();
                    if (timeout) {
                        if (Answer.toUpperCase().equals(getResources().getString(R.string.prelimsanswer11))) {
                            countDownTimer.cancel();

                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putString("score1", String.valueOf(Score));
                            editor.putString("time1", time);
                            editor.apply();

                            UserData userData = new UserData();
                            userData.setId(savedid);
                            userData.setName(savedname);

                            userData.setScore1(sharedPreferences.getString("score1", null));
                            userData.setTime1(sharedPreferences.getString("time1", null));
                            userData.setScore2("");
                            userData.setTime2("");
                            userData.setScore3("");
                            userData.setTime3("");

                            user = reference.child("Users");
                            user.child(savedname + savedid).setValue(userData);


                            Toast.makeText(prelims.this, "Correct answer", Toast.LENGTH_SHORT).show();

                            Intent intent = new Intent(prelims.this, prilims2.class);
                            startActivity(intent);

                        } else {
                            Toast.makeText(prelims.this, "Wrong answer", Toast.LENGTH_SHORT).show();
                        }
                    }
                    else
                    {
                        Toast.makeText(prelims.this, "Time out", Toast.LENGTH_SHORT).show();
                    }
                }
                else
                {
                    Toast.makeText(prelims.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }
            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if(Score == 0)
                {

                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("score1","0");
                    editor.putString("time1","");
                    editor.apply();

                    UserData userData = new UserData();
                    userData.setId(savedid);
                    userData.setName(savedname);

                    userData.setScore1(sharedPreferences.getString("score1", null));
                    userData.setTime1(sharedPreferences.getString("time1", null));
                    userData.setScore2("");
                    userData.setTime2("");
                    userData.setScore3("");
                    userData.setTime3("");

                    user = reference.child("Users");
                    user.child(savedname+savedid).setValue(userData);

                    Intent intent = new Intent(prelims.this,prilims2.class);
                    startActivity(intent);
                }
                else
                {
                    Toast.makeText(prelims.this, "Please wait", Toast.LENGTH_SHORT).show();
                }
            }
        });

        if(clue == 1)
        {
            Score = 25;
            StartTimer();
        }
        else if(clue == 2)
        {
            Score = 20;
            setclue(clue);

        }
        else if(clue == 3)
        {
            Score = 15;
            setclue(clue);
        }
        else if(clue == 4)
        {
            Score = 10;
            setclue(clue);
        }
        else if(clue == 5)
        {
            Score = 5;
            setclue(clue);
        }

    }

    private void setclue(Integer clue)
    {
        if(clue == 2)
        {
            clue2.requestLayout();
            clue2.requestFocus(0);
            layoutParams2.height= ViewGroup.LayoutParams.WRAP_CONTENT;
            StartTimer();
        }
        else if(clue == 3)
        {
            clue2.requestLayout();
            clue2.requestFocus(0);
            layoutParams2.height= ViewGroup.LayoutParams.WRAP_CONTENT;
            clue3.requestLayout();
            clue3.requestFocus();
            layoutParams3.height= ViewGroup.LayoutParams.WRAP_CONTENT;
            StartTimer();
        }
        else if(clue == 4)
        {
            clue2.requestLayout();
            clue2.requestFocus(0);
            layoutParams2.height= ViewGroup.LayoutParams.WRAP_CONTENT;
            clue3.requestLayout();
            clue3.requestFocus();
            layoutParams3.height= ViewGroup.LayoutParams.WRAP_CONTENT;
            clue4.requestLayout();
            clue4.requestFocus();
            layoutParams4.height= ViewGroup.LayoutParams.WRAP_CONTENT;
            StartTimer();
        }
        else if(clue == 5)
        {
            clue2.requestLayout();
            clue2.requestFocus(0);
            layoutParams2.height= ViewGroup.LayoutParams.WRAP_CONTENT;
            clue3.requestLayout();
            clue3.requestFocus();
            layoutParams3.height= ViewGroup.LayoutParams.WRAP_CONTENT;
            clue4.requestLayout();
            clue4.requestFocus();
            layoutParams4.height= ViewGroup.LayoutParams.WRAP_CONTENT;
            clue5.requestLayout();
            clue5.requestFocus();
            layoutParams5.height= ViewGroup.LayoutParams.WRAP_CONTENT;
            StartTimer();
        }
    }

    public  void StartTimer()
    {
//
        countDownTimer = new CountDownTimer(30000,1000)
        {

            @Override
            public void onTick(long millisUntilFinished)
            {
                time = String.format(Locale.getDefault(),"%02d min : %02d sec",
                        TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished)%60,
                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished)%60);
                timer.setText(time);
            }

            @Override
            public void onFinish()
            {
                Score -= 5;
                if(Score == 20)
                {
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putInt("clue1",2);
                    editor.apply();
                    setclue(2);
                }
                else if(Score == 15)
                {
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putInt("clue1",3);
                    editor.apply();
                    setclue(3);
                }
                else if(Score == 10)
                {
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putInt("clue1",4);
                    editor.apply();
                    setclue(4);
                }
                else if(Score == 5)
                {
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putInt("clue1",5);
                    editor.apply();
                    setclue(5);
                }
                else if(Score == 0)
                {
                    timer.setText("Time out");
                    timeout = false;

                }

            }
        }.start();
    }
    @Override
    public void onBackPressed()
    {
    }
}
