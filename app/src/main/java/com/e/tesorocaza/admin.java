package com.e.tesorocaza;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.firebase.client.Firebase;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class admin extends AppCompatActivity {

    DatabaseReference reference,round;
    Firebase firebase;
    public static final String Firebase_Server_URL = "https://tesorocaza-fe7a5.firebaseio.com/";

    RadioGroup radioGroup;
    RadioButton prilms,finals,availability;
    String getvalue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);

        if(!InternetConnection.checkConnection(admin.this))
        {
            Toast.makeText(this, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
        radioGroup = findViewById(R.id.radioGroup);
        radioGroup.clearCheck();
        prilms = findViewById(R.id.prilims);
        finals = findViewById(R.id.finals);
        availability = findViewById(R.id.availability);


        Firebase.setAndroidContext(admin.this);
        firebase = new Firebase(Firebase_Server_URL);
        reference = FirebaseDatabase.getInstance().getReference();
        round = reference.child("Round");

        round.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                getvalue = dataSnapshot.getValue(String.class);
                Toast.makeText(admin.this, getvalue, Toast.LENGTH_SHORT).show();
                if(getvalue.equals("Prilims"))
                {
                    prilms.setChecked(true);
                }
                else  if (getvalue.equals("Finals"))
                {
                    finals.setChecked(true);
                }
                else  if (getvalue.equals("Unavailable"))
                {
                    availability.setChecked(true);
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                if(prilms.isChecked())
                {
                    String value = prilms.getText().toString().trim();
                    round.setValue(value);
                }
                else if(finals.isChecked())
                {
                    String value = finals.getText().toString().trim();
                    round.setValue(value);
                }
                else if(availability.isChecked())
                {
                    String value = availability.getText().toString().trim();
                    round.setValue(value);
                }
            }
        });
    }
}
