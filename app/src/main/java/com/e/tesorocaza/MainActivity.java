package com.e.tesorocaza;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.firebase.client.Firebase;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class MainActivity extends AppCompatActivity {
    Button login;
    EditText password,id,name;
    String Password,Id,Name,getvalue;

    DatabaseReference reference,round,user;
    Firebase firebase;
    public static final String Firebase_Server_URL = "https://tesorocaza-fe7a5.firebaseio.com/";
    SharedPreferences sharedPreferences ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sharedPreferences = getSharedPreferences("DATA", Context.MODE_PRIVATE);

        Firebase.setAndroidContext(MainActivity.this);
        firebase = new Firebase(Firebase_Server_URL);
        reference = FirebaseDatabase.getInstance().getReference();
        round = reference.child("Round");
        getvalue = "xxxx";
        round.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                getvalue = dataSnapshot.getValue(String.class);
                Toast.makeText(MainActivity.this, getvalue, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        if(InternetConnection.checkConnection(MainActivity.this))
        {
//            Toast.makeText(this, "Internet Available", Toast.LENGTH_SHORT).show();
        }
        else
        {
            Toast.makeText(this, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
        Password = "";
        Name = "";
        Id = "";

        id = (EditText)findViewById(R.id.id);
        name = (EditText)findViewById(R.id.name);
        password =(EditText) findViewById(R.id.password);
        login =(Button) findViewById(R.id.login);


        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Id = id.getText().toString().trim();
                Name = name.getText().toString().trim();
                Password = password.getText().toString().trim();

                if(InternetConnection.checkConnection(MainActivity.this))
                {
                    if(Id.toUpperCase().equals(getResources().getString(R.string.admin)) && Name.toUpperCase().equals(getResources().getString(R.string.admin)) && Password.toUpperCase().equals(getResources().getString(R.string.adminpassword)))
                    {
                        Intent intent = new Intent(MainActivity.this, admin.class);
                        startActivity(intent);
                    }
                    else if(Id.equals(""))
                    {
                        if(Name.equals(""))
                        {
                            if(Password.equals(""))
                            {
                                Toast.makeText(MainActivity.this, "Please fill the form", Toast.LENGTH_SHORT).show();
                                id.setError("Type your Id");
                                name.setError("Type your Name");
                                password.setError("Type your Password");
                            }
                            else
                            {
                                Toast.makeText(MainActivity.this, "Please fill the form..", Toast.LENGTH_SHORT).show();
                                id.setError("Type your Id");
                                name.setError("Type your Name");
                            }

                        }
                        else
                        {
                            Toast.makeText(MainActivity.this, "Please fill the form..", Toast.LENGTH_SHORT).show();
                            id.setError("Type your Id");
                        }
                    }
                    else if (Name.equals(""))
                    {
                        if(Password.equals(""))
                        {
                            Toast.makeText(MainActivity.this, "Please fill the form..", Toast.LENGTH_SHORT).show();
                            name.setError("Type your Name");
                            password.setError("Type your Password");
                        }
                        else
                        {
                            Toast.makeText(MainActivity.this, "Please fill the form..", Toast.LENGTH_SHORT).show();
                            name.setError("Type your Name");
                        }
                    }
                    else if(Password.equals(""))
                    {
                        Toast.makeText(MainActivity.this, "Please enter password", Toast.LENGTH_SHORT).show();
                        password.setError("Type your Password");
                    }
                    else {
//                        Toast.makeText(MainActivity.this, "Success", Toast.LENGTH_SHORT).show();
                        if (Id.length() == 7) {
                            if (Id.toUpperCase().substring(0, 3).equals("EXC"))
                            {
                                if(Password.toUpperCase().equals(getResources().getString(R.string.userpassword)))
                                {


                                    if(getvalue.equals("Prilims"))
                                    {
                                        UserData userData = new UserData();
                                        userData.setId(Id);
                                        userData.setName(Name);
                                        user = reference.child("Users");
                                        user.child(Name+Id).setValue(userData);

                                        SharedPreferences.Editor editor = sharedPreferences.edit();
                                        editor.putString("id",Id);
                                        editor.putString("name",Name);
                                        editor.apply();

                                        Intent intent = new Intent(MainActivity.this,instructions.class);
                                        intent.putExtra("round","Prilims");
                                        startActivity(intent);
                                    }
                                    else if(getvalue.equals("Finals"))
                                    {
                                        Intent intent = new Intent(MainActivity.this,instructions.class);
                                        intent.putExtra("round","Finals");
                                        startActivity(intent);
                                    }
                                    else if(getvalue.equals("Unavailable"))
                                    {
                                        Toast.makeText(MainActivity.this, " Service Unavailable", Toast.LENGTH_SHORT).show();
                                    }
                                    else if(getvalue.equals("xxxx"))
                                    {
                                        Toast.makeText(MainActivity.this, "Try again..", Toast.LENGTH_SHORT).show();
                                    }
                                }
                                else
                                {
                                    Toast.makeText(MainActivity.this, "Wrong password", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(MainActivity.this, "Invalid Id", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(MainActivity.this, "Invalid Id", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                else
                {
                    Toast.makeText(MainActivity.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
