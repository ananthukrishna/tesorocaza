package com.e.tesorocaza;

public class UserData
{
    private String name;
    private String id;
    private String score1;
    private String time1;
    private String score2;
    private String time2;
    private String score3;
    private String time3;
    private String total;

    UserData() {
        // This is default constructor.
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {

        this.name = name;
    }

    public String getId() {
        return id;
    }
    public void setId(String id) {

        this.id = id;
    }
    public String getScore1() {
        return score1;
    }

    public void setScore1(String score1) {

        this.score1 = score1;
    }
    public String getTime1() {
        return time1;
    }

    public void setTime1(String time1) {

        this.time1 = time1;
    }
    public String getScore2() {
        return score2;
    }

    public void setScore2(String score2) {

        this.score2 = score2;
    }
    public String getTime2() {
        return time2;
    }

    public void setTime2(String time2) {

        this.time2 = time2;
    }
    public String getScore3() {
        return score3;
    }

    public void setScore3(String score3) {

        this.score3 = score3;
    }
    public String getTime3() {
        return time3;
    }

    public void setTime3(String time3) {

        this.time3 = time3;
    }
    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {

        this.total = total;
    }



}
